***
# Automate-Powerpoint-Creation

## Description

The python script **main.py** automates the creation of a Microsoft Powerpoint file. The script relies on [python-pptx library](https://python-pptx.readthedocs.io/en/latest/), a python library for creating and updating powerpoint files. 

This is a simple demo that create new Powerpoint files based on existing Powerpoint template files. The Powerpoint template files are located inside the *pptx-templates* folder. The outputs is stored in the *pptx-output* folder. 

## Requirement
- Python
- PIP (Package Installer for Python)

## Dependency
- python-pptx library

## Setup
1. Install Python
2. Install PIP
3. Download repo
4. Open repo in IDE and create a virtual environment using .venv in the root folder
5. Install python-pptx library
6. Run python script
7. Output file in stored in *pptx-output* folder

## Backstory
I was tasked with creating weekly report on certain technology (database/network/servers) metrics. This required me to access servers and devices to manually collect these metrics. Since the task is repetitive and the procedure is pretty much the same, I decided to automate it with python script.
The summary of the job is as follows
- Every Friday a cron job runs and collect metrics on technology infrastructure
- Python script picks up this metrics and creates powerpoint reports
- Report is verified and sent via email to manager as weekly update. 

## Future Update
I hope to upload the full program code sometime in the future. 
