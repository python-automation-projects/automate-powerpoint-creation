import collections.abc
from pptx import Presentation
from pptx.util import Inches

#----------------------------------------------------
#             UPLOAD PPT TEMPLATE
#----------------------------------------------------
prs = Presentation('pptx-templates/template.pptx')

#----------------------------------------------------
#                    SLIDE 1
#----------------------------------------------------
title_only_slide_layout = prs.slide_layouts[0]
slide = prs.slides.add_slide(title_only_slide_layout)
title = slide.shapes.title
subtitle1 = slide.placeholders[1]
subtitle13 = slide.placeholders[13]
subtitle14 = slide.placeholders[14]
subtitle15 = slide.placeholders[15]

title.text = "Apple Services Limited"
subtitle1.text = "Discussion Document"
subtitle13.text = "Proposal"
subtitle14.text = "10/05/2023"
subtitle15.text = "Microsoft UK"

#----------------------------------------------------
#                    SLIDE 2
#----------------------------------------------------
title_only_slide_layout = prs.slide_layouts[1]
slide = prs.slides.add_slide(title_only_slide_layout)
title = slide.shapes.title
subtitle13 = slide.placeholders[13]

title.text = "Introduction"
subtitle13.text = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "'de Finibus Bonorum et Malorum'" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "'Lorem ipsum dolor sit amet..'", comes from a line in section 1.10.32."

#----------------------------------------------------
#                    SLIDE 3
#----------------------------------------------------
slide = prs.slides.add_slide(prs.slide_layouts[2])
slide.shapes.title.text = "Security assessment"
placeholder = slide.placeholders[13]

#set number of table rows and columns
rows = 2
cols = 4

graphic_frame = placeholder.insert_table(rows, cols)
table = graphic_frame.table

# set column widths
table.columns[0].width = Inches(2.0)
table.columns[1].width = Inches(2.0)
table.columns[2].width = Inches(2.0)
table.columns[3].width = Inches(4.0)

# write column headings
table.cell(0, 0).text = 'Clause'
table.cell(0, 1).text = 'Control'
table.cell(0, 2).text = 'Requirement'
table.cell(0, 3).text = 'Recommendation'

# write body cells
table.cell(1, 0).text = 'Human Resources'
table.cell(1, 1).text = 'Qux'
table.cell(1, 0).text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
table.cell(1, 1).text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"

#----------------------------------------------------
#           SAVE AND GENERATE PPT FILE
#----------------------------------------------------
prs.save('pptx-outputs/presentation.pptx')
